import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Test2 {
    public static void main(String[] args) {
        System.out.println("Testing On Firefox");
        System.setProperty("webdriver.gecko.driver","D:\\SW\\geckodriver.exe");
        FirefoxOptions options = new FirefoxOptions();
        options.setBinary("C:\\Program Files\\Mozilla Firefox\\firefox.exe"); //This is the location where you have installed Firefox on your machine
        //options.setHeadless(true);

        WebDriver driver = new FirefoxDriver(options);
        //driver.get("https://angular.io/");
        driver.get("https://teradata.github.io/covalent/#/");
        System.out.println(driver.getPageSource());

        //driver.findElement(By.xpath("/html/body/docs-covalent/td-layout/mat-sidenav-container/mat-sidenav-content/app-home/td-layout-nav/div/div/div/section[1]/div/div[1]/div[2]/button")).sendKeys("alpesh");
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/docs-covalent/td-layout/mat-sidenav-container/mat-sidenav-content/app-home/td-layout-nav/div/div/div/section[1]/div/div[1]/div[2]/button")));
        driver.findElement(By.xpath("/html/body/docs-covalent/td-layout/mat-sidenav-container/mat-sidenav-content/app-home/td-layout-nav/div/div/div/section[1]/div/div[1]/div[2]/button")).click();

        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

        //WebDriverWait wait1 = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("/html/body/docs-covalent/td-layout/mat-sidenav-container/" +
                "mat-sidenav-content/app-docs/td-layout-nav-list/div/mat-sidenav-container/mat-sidenav-content/div/div/div/docs-overview/td-readme-loader/mat-card/mat-card-content/td-pretty-markdown/td-markdown[1]/div/p[2]/a"),"material getting started"));

        System.out.println(driver.getPageSource());

        //driver.quit();
    }
}
