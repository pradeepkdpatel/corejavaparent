import com.gargoylesoftware.htmlunit.BrowserVersion;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class WebScraping {

    public static void main(String[] args) {
        // Declaring and initialising the HtmlUnitWebDriver
        HtmlUnitDriver unitDriver = new HtmlUnitDriver(BrowserVersion.FIREFOX_38);
        unitDriver.setJavascriptEnabled(true);
        unitDriver.get("http://google.com");
        String domainName = (String) unitDriver
                .executeScript("return document.domain");
        System.out.println("Domain name is " + domainName);
    }
}
