import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class HeadlessBrowseInFFandChrome {
    public static void main(String[] args) {

        System.out.println("Testing On Firefox");
        System.setProperty("webdriver.gecko.driver","D:\\SW\\geckodriver.exe");
        FirefoxOptions options = new FirefoxOptions();
        //This is the location where you have installed Firefox on your machine
        options.setBinary("C:\\Program Files\\Mozilla Firefox\\firefox.exe");
        options.setHeadless(true);

        WebDriver driver = new FirefoxDriver(options);
        //driver.get("https://angular.io/");
        driver.get("https://teradata.github.io/covalent/#/");
        System.out.println(driver.getPageSource());

        System.out.println("***************************************");

        System.out.println("Testing On Chrome");
        System.setProperty("webdriver.chrome.driver","D:\\SW\\chromedriver.exe");
        ChromeOptions options1 = new ChromeOptions();
        //This is the location where you have installed Chrome on your machine
        options1.setBinary("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
        options1.setHeadless(true);

        WebDriver driver1 = new ChromeDriver(options1);
        //driver.get("https://angular.io/");
        driver1.get("https://teradata.github.io/covalent/#/");
        System.out.println(driver1.getPageSource());

        System.out.println("**********************");
        //"C:\Program Files\internet explorer\iexplore.exe"
    }
}
