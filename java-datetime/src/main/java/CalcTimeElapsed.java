import java.time.Instant;
import java.util.concurrent.TimeUnit;

public class CalcTimeElapsed {
    public static void main(String[] args) {
        // calculate elapsed time in milli seconds
        Long startTime = Instant.now().toEpochMilli();
        try {
            // do your business logic here
            doYourBusinessLogic();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Long endTime = Instant.now().toEpochMilli();
        Long elapsedTime = endTime - startTime;
        System.out.println("Elapsed time in milli seconds: "+elapsedTime);

        // calculate elapsed time in seconds
        Long stTimeInSec = Instant.now().getEpochSecond();
        try {
            // do your business logic here
            doYourBusinessLogic();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Long endTimeInSec = Instant.now().getEpochSecond();

        elapsedTime = endTimeInSec - stTimeInSec;
        System.out.println("Elapsed time in seconds: "+elapsedTime);
    }

    private static void doYourBusinessLogic() throws InterruptedException {

        TimeUnit.SECONDS.sleep(3);
    }
}
