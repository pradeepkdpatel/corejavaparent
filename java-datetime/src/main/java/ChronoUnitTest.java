import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class ChronoUnitTest {
    public static void main(String[] args) {

        LocalDateTime initialDate = LocalDateTime.now().minusMinutes(5);
        long minutes = ChronoUnit.MINUTES.between(initialDate , LocalDateTime.now());
        System.out.println("In minutes->"+minutes);

    }
}
