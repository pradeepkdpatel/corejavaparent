package com.patel.pradeep;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;

public class Test {
    public static void main(String[] args) {
        File file = new File("D:\\SW\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");
        System.setProperty("phantomjs.binary.path", file.getAbsolutePath());

        Capabilities caps = new DesiredCapabilities();

        ((DesiredCapabilities) caps).setJavascriptEnabled(true);

       // ((DesiredCapabilities) caps).setCapability(“takesScreenshot”, true);

        //((DesiredCapabilities) caps).setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,  "D:\\SW\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");

        //((DesiredCapabilities) caps).setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});

        PhantomJSDriver driver = new PhantomJSDriver(caps);
//driver.executeScript("return document.documentElement.outerHTML");
        driver.get("https://angular.io/");

        System.out.println(driver.getPageSource());
        System.out.println("***************************");
       /* WebElement element = driver.findElement(By.name("q"));
        element.sendKeys("Guru99");
        element.submit();
        System.out.println("Page title is: " + driver.getTitle());*/
        driver.quit();
    }
}
