package com.patel.pradeep;
//http://www.codejava.net/coding/how-to-read-excel-files-in-java-using-apache-poi

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

import static org.apache.poi.ss.usermodel.CellType.*;

/**
 * A dirty simple program that reads an Excel file.
 * @author www.codejava.net
 */
public class SimpleExcelReaderExample {
    public static void main(String[] args) throws IOException {
        String excelFilePath = "JavaBooks4BeginnerPrice.xlsx";
        try (FileInputStream inputStream = new FileInputStream(new File(excelFilePath))) {

            Workbook workbook = new XSSFWorkbook(inputStream);
            FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
            Sheet firstSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = firstSheet.iterator();

            while (iterator.hasNext()) {
                Row nextRow = iterator.next();
                Iterator<Cell> cellIterator = nextRow.cellIterator();

                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();

                    switch (cell.getCellType()) {
                        case STRING:
                            System.out.print(cell.getStringCellValue());
                            break;
                        case BOOLEAN:
                            System.out.print(cell.getBooleanCellValue());
                            break;
                        case NUMERIC:
                            System.out.print(cell.getNumericCellValue());
                            break;
                        case FORMULA:
                            //System.out.print(cell.getStringCellValue());
                            System.out.print(cell.getCellFormula());
                            System.out.print(evaluator.evaluate(cell).getNumberValue());
                            //System.out.print(cell.getCellFormula().toString());
                            break;
                    }
                    System.out.print(" - ");
                }
                System.out.println();
            }

            workbook.close();
        }
    }
}
