package com.patel.pradeep;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
/**
 * This program demonstrates reading an Excel document, add a formula
 * to the first sheet, and then save the file.
 * @author www.codejava.net
 *  NOT_WORKING
 */
public class ExcelFormulaUpdateDemo {
    public static void main(String[] args) throws IOException {
        String excelFilePath = "JavaBooks4BeginnerPrice.xlsx";
        FileInputStream inputStream = new FileInputStream(new File(excelFilePath));

        FileOutputStream outputStream;
        try (Workbook workbook = WorkbookFactory.create(inputStream)) {
            Sheet sheet = workbook.getSheetAt(0);

            sheet.getRow(6).getCell(4).setCellFormula("SUM(D2:D5) + SUM(D2:D5)");

            inputStream.close();

            outputStream = new FileOutputStream(excelFilePath);
            workbook.write(outputStream);
        }
        outputStream.close();
    }
}
