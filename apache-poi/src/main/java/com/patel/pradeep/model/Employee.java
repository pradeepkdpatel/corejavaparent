package com.patel.pradeep.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public record Employee(String name, Integer age, String address, Double salary) {
    public static void main(String[] args) {
        List<Employee> list = new ArrayList<>();
        Employee emp = new Employee("Hello", 15, "World", 10.5);
        Employee emp1 = new Employee("Hello", 4, "World", 18.5);
        Employee emp2 = new Employee("Hello", 77, "World", 8.5);
        list.add(emp);
        list.add(emp1);
        list.add(emp2);
        System.out.println(list);
        Collections.sort(list, Comparator.comparingInt(o -> o.age));
        System.out.println(list);
        Collections.sort(list, Comparator.comparingInt(Employee::age).reversed());
        System.out.println(list);
    }
}
