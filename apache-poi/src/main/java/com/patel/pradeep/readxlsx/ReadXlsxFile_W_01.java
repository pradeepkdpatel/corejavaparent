package com.patel.pradeep.readxlsx;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;

// Reading an excel file using POI
// https://howtodoinjava.com/java/library/readingwriting-excel-files-in-java-poi-tutorial/
public class ReadXlsxFile_W_01 {
    public static void main(String[] args) {
        readFile(Paths.get("D:\\tmp\\abc.xlsx"));
    }

    public static void readFile(Path path) {
        try {
//            try (FileInputStream file = new FileInputStream(new File("howtodoinjava_demo.xlsx"))) {
            try (FileInputStream file = new FileInputStream(path.toFile())) {

                //Create Workbook instance holding reference to .xlsx file
                XSSFWorkbook workbook = new XSSFWorkbook(file);

                FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();

                //Get first/desired sheet from the workbook
                XSSFSheet sheet = workbook.getSheetAt(0);

                //Iterate through each rows one by one
                Iterator<Row> rowIterator = sheet.iterator();

                // reads all data************************************
                while (rowIterator.hasNext()) {
                    Row row = rowIterator.next();
                    for(int i=0;i<row.getLastCellNum();i++){
                        if(null != row.getCell(i)) {
                            extracted(row.getCell(i), row.getCell(i).getCellType());
                        }else{
                            System.out.print("Blank" + "3\t\t");
                        }
                    }

                    Iterator<Cell> cellIterator = row.cellIterator();

                    System.out.println("");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void extracted(Cell cell, CellType cellType) {
        switch (cellType) {
            case NUMERIC:
                System.out.print(cell.getNumericCellValue() + "1\t\t");
                break;
            case STRING:
                System.out.print(cell.getStringCellValue() + "2\t\t");
                break;
            case FORMULA:
                System.out.print("FORMULA");
                break;
            case BLANK:
                System.out.print("Blank" + "3\t\t");
                break;
            case BOOLEAN:
                System.out.print(cell.getBooleanCellValue() + "4\t\t");
                break;
            case _NONE:
                System.out.print("None" + "5\t\t");
                break;
            case ERROR:
                System.out.print("ERROR" + "6\t\t");
                break;
            default:
                System.out.print("DEFAULT" + "7\t\t");
                break;
        }
    }

}
