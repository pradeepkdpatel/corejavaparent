package com.patel.pradeep.readxlsx;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Iterator;

// Reading an excel file using POI
// https://howtodoinjava.com/java/library/readingwriting-excel-files-in-java-poi-tutorial/
public class ReadXlsxFile {
    public static void main(String[] args) {
        readFile(Paths.get("D:\\tmp\\abc.xlsx"));
    }

    public static void readFile(Path path) {
        try {
//            try (FileInputStream file = new FileInputStream(new File("howtodoinjava_demo.xlsx"))) {
            try (FileInputStream file = new FileInputStream(path.toFile())) {

                //Create Workbook instance holding reference to .xlsx file
                XSSFWorkbook workbook = new XSSFWorkbook(file);

                FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();

                //Get first/desired sheet from the workbook
                XSSFSheet sheet = workbook.getSheetAt(0);
              /*  System.out.println(sheet.getLastRowNum());
                System.out.println(sheet.getFirstRowNum());
                System.out.println(sheet.getDefaultColumnWidth());
                System.out.println(Arrays.toString(sheet.getColumnBreaks()));*/

                //Iterate through each rows one by one
                Iterator<Row> rowIterator = sheet.iterator();

                // reads all data************************************
                while (rowIterator.hasNext()) {
                    Row row = rowIterator.next();
                    for(int i=0;i<row.getLastCellNum();i++){
                        if(null != row.getCell(i)) {
                            extracted(row.getCell(i), row.getCell(i).getCellType());
                        }else{
                            System.out.print("Blank" + "3\t\t");
                        }
                    }


                    /*System.out.println(row.getFirstCellNum());
                    System.out.println(row.getLastCellNum());*/
                    /*System.out.println(row.getLastCellNum());
                    System.out.println(row.getHeight());
                    System.out.println(row.getHeight());
                    System.out.println(row.getPhysicalNumberOfCells());
                    System.out.println(row.getRowNum());*/

                    
                    
                    
                    
                    
//                    System.out.println("AAAA"+row);
                    //For each row, iterate through all the columns
                    Iterator<Cell> cellIterator = row.cellIterator();

                    /*while (cellIterator.hasNext()) {
//                        System.out.println("BBB"+cellIterator);
                        Cell cell = cellIterator.next();
//                        System.out.println(cell.getCellType().ordinal()+" |CELLINDEX=>"+cell.getColumnIndex());
                        //Check the cell type after eveluating formulae
                        //If it is formula cell, it will be evaluated otherwise no change will happen
//                        var type = cell.getCellType();
//                        System.out.println(type);
//                        System.out.println(cell.getCellType().getDeclaringClass().descriptorString());
                        CellType cellType = evaluator.evaluateInCell(cell).getCellType();
//                        try {
//                            System.out.println("ZZZ|" + cell.getNumericCellValue() + "|PPP");
//                        }catch (IllegalStateException e){
//                            System.out.println(e.getMessage());
//                        }
//                        try {
//                            System.out.println("ZZZ|" + cell.getStringCellValue() + "|PPP");
//                        }catch (IllegalStateException e){
//                            System.out.println(e.getMessage());
//                        }
                        extracted(cell, cellType);
                    }*/
                    System.out.println("");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void extracted(Cell cell, CellType cellType) {
        switch (cellType) {
            //Check the cell type and format accordingly
//                        switch (cell.getCellType()) {
//                            case Cell.CELL_TYPE_NUMERIC:
            case NUMERIC:
                System.out.print(cell.getNumericCellValue() + "1\t\t");
                break;
//                            case Cell.CELL_TYPE_STRING:
            case STRING:
                System.out.print(cell.getStringCellValue() + "2\t\t");
                break;
            case FORMULA:
                System.out.print("FORMULA");
                break;
            case BLANK:
                System.out.print("Blank" + "3\t\t");
                break;
            case BOOLEAN:
                System.out.print(cell.getBooleanCellValue() + "4\t\t");
                break;
            case _NONE:
                System.out.print("None" + "5\t\t");
                break;
            case ERROR:
                System.out.print("ERROR" + "6\t\t");
                break;
            default:
                System.out.print("DEFAULT" + "7\t\t");
                break;
        }
    }

}
