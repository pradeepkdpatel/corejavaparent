package com.patel.pradeep.readxlsx;

import com.patel.pradeep.model.Employee;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

// Reading an excel file using POI
// https://howtodoinjava.com/java/library/readingwriting-excel-files-in-java-poi-tutorial/
public class ReadXlsxFile_Create_Object {
    public static void main(String[] args) {
        readFile(Paths.get("D:\\tmp\\abc.xlsx"));
    }

    public static void readFile(Path path) {
        try {
//            try (FileInputStream file = new FileInputStream(new File("howtodoinjava_demo.xlsx"))) {
            try (FileInputStream file = new FileInputStream(path.toFile())) {

                //Create Workbook instance holding reference to .xlsx file
                XSSFWorkbook workbook = new XSSFWorkbook(file);

                FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();

                //Get first/desired sheet from the workbook
                XSSFSheet sheet = workbook.getSheetAt(0);

                //Iterate through each rows one by one
                Iterator<Row> rowIterator = sheet.iterator();
                List<Employee> empList = new ArrayList<>();
                // reads all data************************************
                while (rowIterator.hasNext()) {
                    Row row = rowIterator.next();
                    //SKIP HEADER
                    System.out.println(row.getFirstCellNum());
                    int columnLength = row.getLastCellNum();
                    Map<Integer, Object> map = new HashMap<>();
                    for(int i=0;i<columnLength;i++){
                        if(null != row.getCell(i)) {
                            map.put(i, extracted(row.getCell(i), row.getCell(i).getCellType()));
                        } else {
                            System.out.print("Blank" + "3\t\t");
                            map.put(i, "BlankPP");
                        }
                    }
                    empList.add(new Employee((String) map.get(1),Integer.valueOf((String) map.get(0)),(String) map.get(2),0.0));
                }

                System.out.println(empList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Object extracted(Cell cell, CellType cellType) {
        switch (cellType) {
            case NUMERIC:
                return cell.getNumericCellValue();
//                break;
            case STRING:
                return cell.getStringCellValue();
//                break;
            case FORMULA:
                System.out.print("FORMULA");
                break;
            case BLANK:
                return "Blank";
//                break;
            case BOOLEAN:
                return cell.getBooleanCellValue();
//                break;
            case _NONE:
                System.out.print("None" + "5\t\t");
                break;
            case ERROR:
                System.out.print("ERROR" + "6\t\t");
                break;
            default:
                return "DEFAULT";
//                break;
        }
        return "AAAA";
    }

}
