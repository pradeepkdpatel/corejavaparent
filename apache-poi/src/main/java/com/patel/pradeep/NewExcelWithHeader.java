package com.patel.pradeep;
//http://www.javahabit.com/2014/04/18/apache-poi-tutorial-reading-writing-excel-files-java/

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class NewExcelWithHeader {
    public static void main(String[] args) {
        //Create a new Workbook
        XSSFWorkbook workbook = new XSSFWorkbook();

        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("Student data");

        //Create the data for the excel sheet
        Map<String, Object[]> data = new TreeMap<>();
        data.put("1", new Object[]{"ID", "FIRSTNAME", "LASTNAME"});
        data.put("2", new Object[]{1, "Randy", "Maven"});
        data.put("3", new Object[]{2, "Raymond", "Smith"});
        data.put("4", new Object[]{3, "Dinesh", "Arora"});
        data.put("5", new Object[]{4, "Barbra", "Klien"});

        //Iterate over data and write it to the sheet
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof String)
                    cell.setCellValue((String) obj);
                else if (obj instanceof Integer)
                    cell.setCellValue((Integer) obj);
            }
        }
        //Save the excel sheet
        try {
            FileOutputStream out = new FileOutputStream(new File("javahabitExcelDemo.xlsx"));
            workbook.write(out);
            out.close();
            System.out.println("javahabitExcelDemo.xlsx Successfully created");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
